#!/usr/bin/bash

#sets pen to green
rosservice call /turtle1/set_pen '0' '255' '0' '4' '0'
#draws the C
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[-7,0.9,0]' '[0,0,3.3]'
#turns off pen and returns turtle to middle 
rosservice call /turtle1/set_pen '0' '0' '0' '1' '1'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[7,-0.9,0]' '[0,0,-3.3]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[1,0,0]' '[0,0,0]'
#sets pen to reddish green
rosservice call /turtle1/set_pen '200' '200' '0' '4' '0'
#draws the  H
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0,-4.2,0]' '[0,0,0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0,2.1,0]' '[0,0,0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[2,0,0]' '[0,0,0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0,-2.1,0]' '[0,0,0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0,4.2,0]' '[0,0,0]'

