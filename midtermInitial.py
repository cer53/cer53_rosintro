# Python 2/3 compatibility imports

from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import numpy as np
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import time

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list



##########################################################
##########################################################
##########################################################
##########################################################
##########################################################
##########################################################
########## Crob doing the tutorial found on Ed  ##########
##########################################################
##########################################################
##########################################################
##########################################################
##########################################################
##########################################################

# All the code for this set up section is just from the tutorial, nothing really to comment on
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

robot = moveit_commander.RobotCommander() # use to get current info 

scene = moveit_commander.PlanningSceneInterface()


group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

# We can get the name of the reference frame for this robot:
planning_frame = move_group.get_planning_frame()
#print("============ Planning frame: %s" % planning_frame)

# We can also print the name of the end-effector link for this group:
eef_link = move_group.get_end_effector_link()
#print("============ End effector link: %s" % eef_link)

# We can get a list of all the groups in the robot:
group_names = robot.get_group_names()
#print("============ Available Planning Groups:", robot.get_group_names())




##########################################################
##########################################################
##########################################################
##########################################################
##########################################################
##########################################################
########## Crob Defining Functions Abstraction  ##########
##########################################################
##########################################################
##########################################################
##########################################################
##########################################################
##########################################################


# THis function that I wrote moves the robot to a safe position to begin my drawing movements
def robotToSafePosition(move_group):

    joint_goal = move_group.get_current_joint_values()
    # safe joint values
    joint_goal[0] = 0
    joint_goal[1] = -1.5707963267948966
    joint_goal[2] = -1.5707963267948966
    joint_goal[3] = -1.5707963267948966
    joint_goal[4] = 0
    joint_goal[5] = 0 

    move_group.go(joint_goal, wait=True)
    # Calling ``stop()`` ensures that there is no residual movement
    move_group.stop()
    print("Done moving from initial position")
    time.sleep(15)
    print("Done stalling, now to begin drawing")



# This function draws my C initial, quite impressively I might add!
# I drew it as a curved C because I thought it had to be, so my code is more complext than 
# it should be
def drawC(move_group, start_angle, end_angle, center_x, center_y, radius, elevated_z):
    print("Beginning to draw C")

    # Creating the waypoints list of points and a linspace of radian angles
    waypoints = []
    num_points = 100
    angles = np.linspace(start_angle, end_angle, num_points)

    # Creating a cartesian path for each angle above
    for angle in angles:
        # Find x, y, and z values for each angle in our angles linspace
        x = center_x + radius * np.cos(angle)
        y = center_y + radius * np.sin(angle)
        z = elevated_z
        # Creates a pose for Cartesian paths and assigns position and orientations for this pose
        pose = move_group.get_current_pose().pose
        pose.position.x = x
        pose.position.y = y
        pose.position.z = z
        pose.orientation.x = 0  
        pose.orientation.y = 0
        pose.orientation.z = 0
        pose.orientation.w = 1.0
        # Adds this pose for this specific segment of the C to our waypoints pose list
        waypoints.append(pose)

    # Plan and execute in smaller segments
    segment_length = 10
    # Loops over the entirety of the waypoints list, jumping up by a segment_length amount each time
    for i in range(0, len(waypoints), segment_length):
        # Takes a smaller segment of the waypoints, the next 5 poses from our waypoints list
        segment = waypoints[i:i+segment_length]
        #print(i)
        # Creates a plan for the robot to move over those poses from our segment list
        (plan, fraction) = move_group.compute_cartesian_path(segment, 0.01, 0.0)
        
        # Prints a warning if not all waypoints in the segment were feasible for path planning.
        if fraction < 1.0:
            print("Warning: Not all waypoints in this segment were feasible!")
        
        # Executes the plan that we have created for the segment of points
        move_group.execute(plan, wait=True)
        time.sleep(1)
    print("Done drawing C") 
    time.sleep(2)


# This function moves my robot to the beginning location before it is about to draw my C
def robotToCStartPoint(move_group, start_angle, end_angle, center_x, center_y, radius, elevated_z):
    # Returning robot back to beginning of the C, after it completes the Cartesian path drawing
    print("Beginning to move robot to C start point")
    # Clears waypoints
    waypoints = []
    # Find x, y, and z values for start angle  = 45
    x = center_x + radius * np.cos(start_angle)
    y = center_y + radius * np.sin(start_angle)
    z = elevated_z
    # Creates a pose and assigns position and orientations for this pose
    pose = move_group.get_current_pose().pose
    pose.position.x = x
    pose.position.y = y
    pose.position.z = z
    pose.orientation.x = 0  
    pose.orientation.y = 0
    pose.orientation.z = 0
    pose.orientation.w = 1.0
    waypoints.append(pose)

    # Computes cartesian path to starting point of my C, and executes the plan
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)
    time.sleep(2)
    print("Finished moving robot to C start point")


# This function moves my robot to the beginning location before it is about to draw my E and R
def robotToERStartPoint(move_group):
    # Moving robot back to beginning of the E
    print("Beginning to move robot to E start point")
    # Clears waypoints
    waypoints = []
    # Find x, y, and z values
    x = 0.3
    y = 0.3
    z = 0.1
    # Creates a pose and assigns position and orientations for this pose
    pose = move_group.get_current_pose().pose
    pose.position.x = x
    pose.position.y = y
    pose.position.z = z
    pose.orientation.x = 0  
    pose.orientation.y = 0
    pose.orientation.z = 0
    pose.orientation.w = 1.0
    waypoints.append(pose)

    # Computes cartesian path to starting point of my C, and executes the plan
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)
    time.sleep(2)
    print("Finished moving robot to E start point")




# This function draws my middle initial, an E
def drawE(move_group):
    print("Beginning to draw E")

    # Generate waypoints for the "E"
    waypoints = []  
    x_positions = [0.3, 0.7, 0.3, 0.3, 0.7, 0.3, 0.3, 0.7]
    y_positions = [0.3, 0.3, 0.3, 0.5, 0.5, 0.5, 0.7, 0.7]
    
    # Creating a cartesian path for each (x,y) coordinate pair above
    for i in range(len(x_positions)):
        x = x_positions[i]
        y = y_positions[i]
        pose = move_group.get_current_pose().pose
        pose.position.x = x
        pose.position.y = y
        pose.position.z = 0.1
        pose.orientation.x = 0
        pose.orientation.y = 0
        pose.orientation.z = 0
        pose.orientation.w = 1.0
        waypoints.append(pose)
    
    # Computes cartesian path to starting point of my C, and executes the plan
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)
    time.sleep(2)
    print("Finished drawing E")
            

# This function draws my last initial, an R
def drawR(move_group):
    print("Beginning to draw R")

    # Generate waypoints for the "E"
    waypoints = []  
    x_positions = [0.3, 0.3, 0.6, 0.6, 0.3, 0.6]
    y_positions = [0.3, 0.7, 0.6, 0.5, 0.5, 0.2]

    # Creating a cartesian path for each (x,y) coordinate pair above
    for i in range(len(x_positions)):
        x = x_positions[i]
        y = y_positions[i]
        pose = move_group.get_current_pose().pose
        pose.position.x = x
        pose.position.y = y
        pose.position.z = 0.1
        pose.orientation.x = 0
        pose.orientation.y = 0
        pose.orientation.z = 0
        pose.orientation.w = 1.0
        waypoints.append(pose)
    
    # Computes cartesian path to starting point of my C, and executes the plan
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    move_group.execute(plan, wait=True)
    time.sleep(2)
    print("Finished drawing R")






##########################################################
##########################################################
##########################################################
##########################################################
##########################################################
##########################################################
############## Christian Midterm Project #################
##########################################################
##########################################################
##########################################################
##########################################################
##########################################################
##########################################################


# Making a center and radius for my "C" first name iniitial that exists in the robot task and workspace
center_x = 0.5
center_y = 0.5
radius = 0.2
elevated_z = 0.1  # Elevating trajectory to avoid singularities

# Having my C go from angle 45 degrees on the unit circle, to angle 315 degrees
start_angle = np.radians(45)
end_angle = np.radians(315)

# The following lines of code show an abstracted view of the individual steps
# that the robot takes to complete the task of drawing all three of my initials
# It uses all of my defined functions above for more intuitive code reading
robotToSafePosition(move_group)
robotToCStartPoint(move_group, start_angle, end_angle, center_x, center_y, radius, elevated_z)
drawC(move_group, start_angle, end_angle, center_x, center_y, radius, elevated_z)
robotToERStartPoint(move_group)
drawE(move_group)
robotToERStartPoint(move_group)
drawR(move_group)
robotToERStartPoint(move_group)
print("============ Printing robot state")
print(robot.get_current_state())
print("")

